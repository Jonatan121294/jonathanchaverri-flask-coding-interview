from db import db


class StudentsModel(db.Model):
    __tablename__ = 'students'

    id = db.Column(db.Integer, primary_key=True)

    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    birth_date = db.Column(db.DateTime)

    is_active = db.Column(db.Boolean, default=True)
    enrollment_complete = db.Column(db.Boolean, default=False)

    


