from flask import Blueprint, request
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from db import db

from apps.student.models import StudentsModel

student_app = Blueprint("student", __name__)


def model_to_dict(model):
    resp = {
        col.name: getattr(model, col.name)
        for col in model.__table__.columns
    }
    return resp


@student_app.route("/api/v1/students", methods=['POST'])
def create_student():
    student_data = request.get_json()
    student = StudentsModel(**student_data)
    try:
        db.session.add(student)
        db.session.commit()
    except IntegrityError:
        return {'message': 'Bad data'}, 400
    except SQLAlchemyError:
        return {'message': 'Failed to insert student'}, 500
    return {'message': 'ok'}


@student_app.route("/api/v1/students")
def get_list():
    students = StudentsModel.query.all()
    resp = [model_to_dict(student) for student in students]
    return resp


@student_app.route("/api/v1/students/<student_id>")
def get_student(student_id):
    student = StudentsModel.query.get(student_id)
    if student:
        return model_to_dict(student)
    return {'message': 'Not found'}, 404


@student_app.route("/api/v1/students/<student_id>", methods=['PUT'])
def edit_student():
    pass
    