from flask_migrate import Migrate

from app import create_app
from db import db

from apps.student.models import StudentsModel

app = create_app()

db.init_app(app)
migrate = Migrate(app, db)

if __name__ == '__main__':
    app.run()
